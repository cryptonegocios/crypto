package tk.bitcryptoclub.fundsmanagement.model;

import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;

import org.apache.commons.text.StringEscapeUtils;

public class CoinInfoPro {
	
	private Integer id;
	
	private String name;
	
	private String symbol;
	
	private Integer rank;
	
	private Double price;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		Currency eur = java.util.Currency.getInstance("EUR");
		NumberFormat format = NumberFormat.getCurrencyInstance(Locale.FRANCE);
		format.setCurrency(eur);

		return "Coin{name=" + name + " (" + symbol + "): " + StringEscapeUtils.escapeHtml4(format.format(price))+ "}";
	}
}
