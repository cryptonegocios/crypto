package tk.bitcryptoclub.fundsmanagement.service;

import tk.bitcryptoclub.fundsmanagement.dto.ValuedFundResponseDTO;

public interface CoinInfoService {
	
	public String valueFundAsString();
	
	public ValuedFundResponseDTO valueFund();

	public ValuedFundResponseDTO valueFundPerCoin();

}
