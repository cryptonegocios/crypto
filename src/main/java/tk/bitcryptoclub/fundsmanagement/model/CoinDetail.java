package tk.bitcryptoclub.fundsmanagement.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("currencies")
public class CoinDetail {
	
	@Id
	private String id;
	
	private String name;
	
	private Double amount;
	
	private String exchange;
	
	private Integer cmcId;
	
	private CoinConversion conversion;

	public CoinDetail() {
	}
	
	public CoinDetail(String id) {
		this.id = id;
	}
	
	public CoinDetail(String id, double amount, String exchange) {
		this(exchange + "_" + id);
		this.amount = amount;
		this.exchange = exchange;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getExchange() {
		return exchange;
	}

	public void setExchange(String exchange) {
		this.exchange = exchange;
	}

	public Integer getCmcId() {
		return cmcId;
	}

	public void setCmcId(Integer cmcId) {
		this.cmcId = cmcId;
	}

	public CoinConversion getConversion() {
		return conversion;
	}

	public void setConversion(CoinConversion conversion) {
		this.conversion = conversion;
	}
}
