package tk.bitcryptoclub.fundsmanagement.service.impl;

import java.text.NumberFormat;
import java.util.Currency;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.text.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tk.bitcryptoclub.fundsmanagement.dto.CoinDetailDTO;
import tk.bitcryptoclub.fundsmanagement.dto.ValuedFundResponseDTO;
import tk.bitcryptoclub.fundsmanagement.model.CoinConversion;
import tk.bitcryptoclub.fundsmanagement.model.CoinDetail;
import tk.bitcryptoclub.fundsmanagement.model.CoinInfoPro;
import tk.bitcryptoclub.fundsmanagement.repository.CoinDetailRepository;
import tk.bitcryptoclub.fundsmanagement.service.CoinInfoService;
import tk.bitcryptoclub.fundsmanagement.service.CoinMarketCapService;

@Service
public class CoinInfoServiceImpl implements CoinInfoService {
	
	@Autowired
	private CoinDetailRepository coinDetailRepository;
	
	@Autowired
	private CoinMarketCapService coinMarketCapService;
	
	public String valueFundAsString() {
		StringBuilder sb = new StringBuilder();
		Currency eur = java.util.Currency.getInstance("EUR");
		NumberFormat format = NumberFormat.getCurrencyInstance(Locale.FRANCE);
		format.setCurrency(eur);
		
		List<CoinDetail> portfolio = coinDetailRepository.findAllByOrderByExchangeAscNameAsc();
		
		Set<Integer> ids = portfolio.stream().map(CoinDetail::getCmcId).filter(Objects::nonNull).collect(Collectors.toSet());
		Map<Integer, CoinInfoPro> quotes = coinMarketCapService.getInfo(ids);
		
		double portfolioValue = 0.0;
		for (CoinDetail detail: portfolio) {
			CoinInfoPro quote;
			if (detail.getCmcId()!=null) {
				quote = quotes.get(detail.getCmcId());
				sb.append(detail.getAmount() + " "  + quote.toString());
				sb.append(" Value: " + StringEscapeUtils.escapeHtml4(format.format(quote.getPrice() * detail.getAmount())) + "</br>");
				portfolioValue += (quote.getPrice() * detail.getAmount());
			} else {
				sb.append(detail.getAmount() + " EUR");
				sb.append(" Value: " + StringEscapeUtils.escapeHtml4(format.format(1 * detail.getAmount())) + "</br>");
				portfolioValue += (1 * detail.getAmount());
			}
		}		
		sb.append("Total amount: " + StringEscapeUtils.escapeHtml4(format.format(portfolioValue)) + "</br>");
		
		return sb.toString();
	}
	
	@Override
	public ValuedFundResponseDTO valueFund() {		
		ValuedFundResponseDTO response = new ValuedFundResponseDTO();
		List<CoinDetail> portfolio = coinDetailRepository.findAllByOrderByExchangeAscNameAsc();
		
		Set<Integer> ids = portfolio.stream().map(CoinDetail::getCmcId).filter(Objects::nonNull).collect(Collectors.toSet());
		
		Map<Integer, CoinInfoPro> quotes = coinMarketCapService.getInfo(ids);
		
		double portfolioValue = 0.0;
		List<CoinDetailDTO> result = new LinkedList<>();
		for (CoinDetail detail: portfolio) {
			CoinDetailDTO coin = new CoinDetailDTO();
			CoinInfoPro quote = null;
			double priceEUR;
			String name = "";
			String symbol = "";
			if (detail.getConversion() == null) {
				quote = quotes.get(detail.getCmcId());
				priceEUR = quote.getPrice();
				name = quote.getName();
				symbol = quote.getSymbol();
			} else {
				CoinConversion cc = detail.getConversion();
				if (cc.getIdRef()!=null) {
					quote = quotes.get(detail.getCmcId());
					priceEUR = quote.getPrice() * cc.getFactor();
					name = cc.getName();
					symbol = cc.getSymbol();
				} else {
					priceEUR = cc.getFactor();
					name = cc.getName();
					symbol = cc.getSymbol();
				}
			}
			coin.setName(name);
			coin.setSymbol(symbol);
			coin.setPrice(priceEUR);
			coin.setValue(detail.getAmount()*priceEUR);
			coin.setAmount(detail.getAmount());
			coin.setExchange(detail.getExchange());
			result.add(coin);
			portfolioValue += (coin.getValue());
		}
		
		response.setValue(portfolioValue);
		response.setDetails(result);
		return response;
	}

	@Override
	public ValuedFundResponseDTO valueFundPerCoin() {
		ValuedFundResponseDTO response = new ValuedFundResponseDTO();
		List<CoinDetail> portfolio = coinDetailRepository.findAllByOrderByNameAsc();
		
		List<CoinDetail> coinList = new LinkedList<>();
		for (CoinDetail detail:portfolio) {
			Optional<CoinDetail> oc = coinList.stream().filter(d -> d.getId().equals(detail.getId())).findFirst();
			if (oc.isPresent()) {
				CoinDetail cd = oc.get();
				cd.setAmount(cd.getAmount()+detail.getAmount());
			} else {
				coinList.add(detail);
			}
			detail.setExchange("");
		}
		
		Set<Integer> ids = coinList.stream().map(CoinDetail::getCmcId).filter(Objects::nonNull).collect(Collectors.toSet());
		Map<Integer, CoinInfoPro> quotes = coinMarketCapService.getInfo(ids);
		
		double portfolioValue = 0.0;
		List<CoinDetailDTO> result = new LinkedList<>();
		for (CoinDetail detail: coinList) {
			CoinInfoPro quote = null;
			CoinDetailDTO coin = new CoinDetailDTO();
			double priceEUR;
			String name = "";
			String symbol = "";
			if (detail.getConversion() == null) {
				quote = quotes.get(detail.getCmcId());
				priceEUR = quote.getPrice();
				name = quote.getName();
				symbol = quote.getSymbol();
			} else {
				CoinConversion cc = detail.getConversion();
				if (cc.getIdRef()!=null) {
					quote = quotes.get(detail.getCmcId());
					priceEUR = quote.getPrice() * cc.getFactor();
					name = cc.getName();
					symbol = cc.getSymbol();
				} else {
					priceEUR = cc.getFactor();
					name = cc.getName();
					symbol = cc.getSymbol();
				}
			}
			coin.setName(name);
			coin.setSymbol(symbol);
			coin.setPrice(priceEUR);
			coin.setValue(detail.getAmount()*priceEUR);
			coin.setAmount(detail.getAmount());
			coin.setExchange(detail.getExchange());
			result.add(coin);
			portfolioValue += (coin.getValue());
		}
		
		response.setValue(portfolioValue);
		response.setDetails(result);
		return response;
	}
}
