package tk.bitcryptoclub.fundsmanagement.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tk.bitcryptoclub.fundsmanagement.dto.ValuedFundResponseDTO;
import tk.bitcryptoclub.fundsmanagement.service.CoinInfoService;


@RestController
@RequestMapping("/funds")
public class FundController {
	
	@Autowired
	private CoinInfoService coinInfoService;

	@GetMapping("/value")
	public ValuedFundResponseDTO getValue() {
		return coinInfoService.valueFund();
	}
	
	@GetMapping("/valueAsString")
	public String getValueAsString() {
		return coinInfoService.valueFundAsString();
	}
	
	@GetMapping("/valuePerCoin")
	public ValuedFundResponseDTO getValuePerCoin() {
		return coinInfoService.valueFundPerCoin();
	}
}
