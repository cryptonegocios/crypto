package tk.bitcryptoclub.fundsmanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class FundsmanagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(FundsmanagementApplication.class, args);
	}
}
