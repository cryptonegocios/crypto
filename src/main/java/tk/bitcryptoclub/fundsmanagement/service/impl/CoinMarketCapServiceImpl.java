package tk.bitcryptoclub.fundsmanagement.service.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import tk.bitcryptoclub.fundsmanagement.model.CoinInfoPro;
import tk.bitcryptoclub.fundsmanagement.service.CoinMarketCapService;

@Service
public class CoinMarketCapServiceImpl implements CoinMarketCapService {

	private static final String PRICE = "price";
	private static final String QUOTE = "quote";
	private static final String RANK = "cmc_rank";
	private static final String SYMBOL = "symbol";
	private static final String NAME = "name";
	private static final String ID = "id";
	private static final String DATA = "data";
	private static final String API_KEY = "75eb2602-2743-464e-83a9-15f59b63ce60";
	private static final String URL_BASE = "https://cmc/v1/cryptocurrency/quotes/latest?id=";
	private static final String TO_EUR = "&convert=";
	private static final String X_CMC_PRO_API_KEY = "X-CMC_PRO_API_KEY";
	private static final String EURO = "EUR";
	
	@Cacheable(value = "cmcCache", key = "'info'")
	@SuppressWarnings("unchecked")
	public Map<Integer, CoinInfoPro> getInfo(Set<Integer> ids) {
		String currencyIds = StringUtils.join(ids, ',');
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.add(X_CMC_PRO_API_KEY, API_KEY);
		HttpEntity<String> entity = new HttpEntity<>("parameters", headers);
		Object o = restTemplate.exchange(URL_BASE + currencyIds + TO_EUR + EURO, HttpMethod.GET, entity, Object.class).getBody();
		
		Map<Integer, CoinInfoPro> infoCurrencies = new HashMap<>();
		ids.forEach(id -> {
			Map<String, Object> info = ((Map<String, Object>)((Map<String, Object>)((Map<String, Object>) o).get(DATA)).get(id.toString()));
			Map<String, Object> quote = ((Map<String, Object>)((Map<String, Object>) info.get(QUOTE)).get(EURO));
			
			CoinInfoPro coinInfo = new CoinInfoPro();
			coinInfo.setId((Integer)info.get(ID));
			coinInfo.setName((String)info.get(NAME));
			coinInfo.setSymbol((String)info.get(SYMBOL));
			coinInfo.setRank((Integer)info.get(RANK));
			coinInfo.setPrice((Double)quote.get(PRICE));
			infoCurrencies.put(id, coinInfo);
		});
		return infoCurrencies;
	}

}
