package tk.bitcryptoclub.fundsmanagement.service;

import java.util.Map;
import java.util.Set;

import tk.bitcryptoclub.fundsmanagement.model.CoinInfoPro;

public interface CoinMarketCapService {

	public Map<Integer, CoinInfoPro> getInfo(Set<Integer> ids);
	
}
