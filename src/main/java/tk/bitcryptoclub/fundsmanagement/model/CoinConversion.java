package tk.bitcryptoclub.fundsmanagement.model;

public class CoinConversion {
	
	private Double factor;
	
	private String idRef;
	
	private String name;
	
	private String symbol;

	public CoinConversion() {

	}
	
	public CoinConversion(double factor) {
		this(factor, null, null, null);
	}
	
	public CoinConversion(Double factor, String idRef, String name, String symbol) {
		this.factor = factor;
		this.idRef = idRef;
		this.name = name;
		this.symbol = symbol;
	}

	public Double getFactor() {
		return factor;
	}

	public void setFactor(Double factor) {
		this.factor = factor;
	}

	public String getIdRef() {
		return idRef;
	}

	public void setIdRef(String idRef) {
		this.idRef = idRef;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
