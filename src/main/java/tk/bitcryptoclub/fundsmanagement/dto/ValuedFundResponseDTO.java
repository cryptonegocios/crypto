package tk.bitcryptoclub.fundsmanagement.dto;

import java.util.List;

public class ValuedFundResponseDTO {

	private double value;
	
	private List<CoinDetailDTO> details;

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public List<CoinDetailDTO> getDetails() {
		return details;
	}

	public void setDetails(List<CoinDetailDTO> details) {
		this.details = details;
	}

}
