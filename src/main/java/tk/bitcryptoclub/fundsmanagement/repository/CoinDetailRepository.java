package tk.bitcryptoclub.fundsmanagement.repository;

import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.mongodb.repository.MongoRepository;

import tk.bitcryptoclub.fundsmanagement.model.CoinDetail;


public interface CoinDetailRepository extends MongoRepository<CoinDetail, String> {
	
	@Cacheable(value="coins", key="'exchanges'")
	public List<CoinDetail> findAllByOrderByExchangeAscNameAsc();
	
	@Cacheable(value="coins", key="'names'")
	public List<CoinDetail> findAllByOrderByNameAsc();
}
